option(
  'build_zip',
  type: 'boolean',
  value: false,
  description: 'Create a ZIP archive of extension files.'
)

option(
  'gschema_dir',
  type: 'string',
  value: '',
  description: 'Set GSettings schema directory.'
)

option(
  'per_user',
  type: 'boolean',
  value: false,
  description: 'Install in per-user mode (not system-wide).'
)