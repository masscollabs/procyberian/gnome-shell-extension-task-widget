#### Feature summary

<!-- 
Describe what you would like to be able to do with the extension
that you currently cannot do.
-->



#### How would you like it to work

<!-- 
If you can think of a way the extension might be able to do this,
let us know here.
-->



#### Relevant links, screenshots, screencasts etc.

<!-- 
If you have further information, such as technical documentation,
code, mockups or a similar feature in another software solutions,
please provide them here.
-->



<!-- Do not remove the following line. -->
/label ~"2. Feature"