msgid ""
msgstr ""
"Project-Id-Version: task-widget\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-10-13 01:39+0200\n"
"PO-Revision-Date: 2020-10-13 00:55\n"
"Last-Translator: \n"
"Language-Team: Lithuanian\n"
"Language: lt_LT\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && (n%100>19 || n%100<11) ? 0 : (n%10>=2 && n%10<=9) && (n%100>19 || n%100<11) ? 1 : n%1!=0 ? 2: 3);\n"
"X-Crowdin-Project: task-widget\n"
"X-Crowdin-Project-ID: 418358\n"
"X-Crowdin-Language: lt\n"
"X-Crowdin-File: /master/data/po/org.gnome.shell.extensions.task-widget.pot\n"
"X-Crowdin-File-ID: 8\n"

#: src/extension.js:97
msgid "Loading"
msgstr "Įkeliama"

#: src/extension.js:206
msgid "Previous task list"
msgstr "Ankstesnis užduočių sąrašas"

#: src/extension.js:242
msgid "Select task list"
msgstr "Pasirinkite užduočių sąrašą"

#: src/extension.js:283 src/extension.js:1203
msgid "All Tasks"
msgstr "Visos užduotys"

#: src/extension.js:312
msgid "Next task list"
msgstr "Sekantis užduočių sąrašas"

#: src/extension.js:689
msgid "Beginning of a subtask list"
msgstr "Užduočių pogrupio pradžia"

#. Translators: this denotes a subtask with an inaccessible
#. parent task.
#: src/extension.js:733
msgid "Orphan task"
msgstr "Pogrupio užduotis be tėvinės užduoties"

#: src/extension.js:752
msgid "No due date"
msgstr "Be termino"

#: src/extension.js:755
msgid "Today"
msgstr "Šiandien"

#. Translators: this is a category name for tasks with
#. due date in the past.
#: src/extension.js:761
msgid "Past"
msgstr "Praeities"

#: src/extension.js:769
msgctxt "task due date"
msgid "%A, %B %-d"
msgstr "%A, %B %-d d."

#: src/extension.js:770
msgctxt "task due date with a year"
msgid "%A, %B %-d, %Y"
msgstr "%A, %Y m. %B %-d d."

#: src/extension.js:1235
msgid "No Tasks"
msgstr "Nėra užduočių"

#: src/extension.js:1241 src/prefs.js:82
msgid "Error: Missing Dependencies"
msgstr "Klaida: trūksta reikalingų modulių"

#: src/prefs.js:83
msgid "Please install Evolution Data Server to use this extension."
msgstr "Kad galėtumėte naudotis šiuo plėtiniu, įrašykite Evolution Data Server."

#: src/prefs.js:236
msgctxt "after X second(s)"
msgid "second"
msgid_plural "seconds"
msgstr[0] "sekundės"
msgstr[1] "sekundžių"
msgstr[2] "sekundės"
msgstr[3] "sekundžių"

#: src/prefs.js:238
msgctxt "after X minute(s)"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minutės"
msgstr[1] "minučių"
msgstr[2] "minutės"
msgstr[3] "minučių"

#: src/prefs.js:239
msgctxt "after X hour(s)"
msgid "hour"
msgid_plural "hours"
msgstr[0] "valandos"
msgstr[1] "valandų"
msgstr[2] "valandos"
msgstr[3] "valandų"

#: src/prefs.js:241
msgctxt "after X day(s)"
msgid "day"
msgid_plural "days"
msgstr[0] "dienos"
msgstr[1] "dienų"
msgstr[2] "dienos"
msgstr[3] "dienų"

#: src/prefs.js:312
msgid "No remote task lists found"
msgstr "Nuotolinių užduočių sąrašų nerasta"

#: src/prefs.js:320 data/ui/settings-window.ui:649
msgid "Refresh the list of account task lists"
msgstr "Atnaujinti užduočių sąrašų sąrašą"

#: src/prefs.js:384
msgid "Refresh in progress"
msgstr "Vyksta atnaujinimas"

#: src/prefs.js:877
msgctxt "refresh every X minutes(s)"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minutę"
msgstr[1] "minutes"
msgstr[2] "minutės"
msgstr[3] "minučių"

#: src/prefs.js:879
msgctxt "refresh every X hour(s)"
msgid "hour"
msgid_plural "hours"
msgstr[0] "valandą"
msgstr[1] "valandas"
msgstr[2] "valandos"
msgstr[3] "valandų"

#: src/prefs.js:881
msgctxt "refresh every X day(s)"
msgid "day"
msgid_plural "days"
msgstr[0] "dieną"
msgstr[1] "dienas"
msgstr[2] "dienos"
msgstr[3] "dienų"

#. Translators: put down your name/nickname and email (optional)
#. according to the format below. This will credit you in the "About"
#. window of the extension settings.
#: src/prefs.js:950
msgctxt "translator name <email>"
msgid "translator-credits"
msgstr "Juozas Miškinis <juozas@joasis.lt>"

#: data/ui/settings-menu.ui:21 data/ui/settings-menu.ui:115
msgid "Support Log"
msgstr "Pagalbinis žurnalas"

#: data/ui/settings-menu.ui:34
msgid "Help"
msgstr "Pagalba"

#: data/ui/settings-menu.ui:47
msgid "About"
msgstr "Apie"

#: data/ui/settings-menu.ui:80
msgid "Copyright © 2020 Juozas Miškinis"
msgstr "Visos teisės saugomos © 2020 Juozas Miškinis"

#: data/ui/settings-menu.ui:116
msgid "Debug messages are being logged. Take any steps necessary to reproduce a problem, then review the log. You should remove or censor any information you consider private."
msgstr "Derinimo pranešimai yra registruojami. Atlikite reikiamus veiksmus, kad pakartotumėte problemą, o tuomet peržiūrėkite žurnalą. Prašome pašalinti ar cenzūruoti informaciją, kurią laikote privačia."

#: data/ui/settings-menu.ui:128 data/ui/task-list-properties-dialog.ui:31
msgid "Cancel"
msgstr "Atšaukti"

#: data/ui/settings-menu.ui:141
msgid "Review Log"
msgstr "Peržiūrėti žurnalą"

#: data/ui/settings-window.ui:53
msgid "Settings"
msgstr "Nustatymai"

#: data/ui/settings-window.ui:95
msgid "Merge task lists"
msgstr "Sulieti užduočių sąrašus"

#: data/ui/settings-window.ui:161
msgid "Group past tasks"
msgstr "Grupuoti praeities užduotis"

#: data/ui/settings-window.ui:228
msgid "Hide header for singular tasks lists"
msgstr "Slėpti vienintelių užduočių sąrašų pavadinimą"

#: data/ui/settings-window.ui:294
msgid "Hide empty and completed task lists"
msgstr "Slėpti tuščius ir užbaigtus užduočių sąrašus"

#: data/ui/settings-window.ui:360
msgid "Hide completed tasks"
msgstr "Slėpti užbaigtas užduotis"

#: data/ui/settings-window.ui:373
msgid "Never"
msgstr "Niekada"

#: data/ui/settings-window.ui:374
msgid "Immediately"
msgstr "Iš karto"

#: data/ui/settings-window.ui:375
msgid "After a period of time after completion"
msgstr "Po tam tikro laiko po užbaigimo"

#: data/ui/settings-window.ui:376
msgid "After a specific time of the day"
msgstr "Po tam tikro paros meto"

#: data/ui/settings-window.ui:415 data/ui/settings-window.ui:465
msgid "After"
msgstr "Po"

#: data/ui/settings-window.ui:557
msgid "Task Lists"
msgstr "Užduočių sąrašai"

#: data/ui/settings-window.ui:574
msgid "You can drag and drop rows to change the order of task lists in the widget."
msgstr "Galite pertempti užduočių sąrašo eilutę ir taip pakeisti jos eiliškumą valdiklyje."

#: data/ui/settings-window.ui:650
msgid "menu"
msgstr "meniu"

#: data/ui/task-list-properties-dialog.ui:13
msgid "Task List Properties"
msgstr "Užduočių sąrašo savybės"

#: data/ui/task-list-properties-dialog.ui:44
msgid "OK"
msgstr "Gerai"

#. Translators: e.g. "Refresh every 10 minutes".
#: data/ui/task-list-properties-dialog.ui:73
msgid "Refresh every"
msgstr "Atnaujinti kas"

#: data/ui/task-list-row.ui:22
msgid "Move Up"
msgstr "Pakelti"

#: data/ui/task-list-row.ui:35
msgid "Move Down"
msgstr "Nuleisti"

#: data/ui/task-list-row.ui:48
msgid "Refresh Tasks"
msgstr "Atnaujinti užduotis"

#: data/ui/task-list-row.ui:61
msgid "Properties"
msgstr "Savybės"

#: data/ui/task-list-row.ui:208
msgid "Task list options"
msgstr "Užduočių sąrašo parinktys"

